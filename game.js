var prompt = require('prompt');
/************************************************************************/
class TicTacToe {
  constructor() {
    this.board = Array(3).fill(['','','']);
    this.player = 'X';
    this.moveCount = 0;
    this.renderBoard = this.renderBoard.bind(this);
    this.makeMove = this.makeMove.bind(this);
    this.determineWinner = this.determineWinner.bind(this)
  }

  renderBoard() {
    console.log(
      '\n',
      this.board[0][0], '|', this.board[0][1],'|', this.board[0][2],
      '\n',
      this.board[1][0], '|', this.board[1][1],'|', this.board[1][2],
      '\n',
      this.board[2][0], '|', this.board[2][1],'|', this.board[2][2]
    );

  }

  makeMove() {
    this.renderBoard();
    console.log(`Player ${this.player}'s turn: choose square 1-9`);
    prompt.get(['square'], function (err, result) {
      if (err) {
        return onErr(err);
      }

      if (!(result.square > 0) && !(result.square < 10)) {
        console.log('Invalid Selection');
        this.makeMove();
      }

      var row = Math.floor((result.square - 1) / 3),
          col = (result.square - 1) % 3;

      if (this.board[row][col] !== '') {
        console.log('Invalid Selection');
        this.makeMove();
      }

      this.board[row][col] = this.player;

      if (this.determineWinner(row, col)) {
        this.renderBoard();
        console.log(`Player ${this.player} Wins, [ctrl + x] and then [npm run game] to play again`);
      }

      this.player = this.player === 'X' ? 'O' : 'X';

      this.makeMove();

    }.bind(this))

  }

  determineWinner(row, col) {
    if (this.board[row][(col + 2) % 3] === this.player && this.board[row][(col + 1) % 3] === this.player) {
      return true;
    }

    if (this.board[(row + 2) % 3][col] === this.player && this.board[(row + 1) % 3][col] === this.player) {
      return true;
    }

    if (row === col && this.board[(row + 2) % 3][(col + 2) % 3] === this.player && this.board[(row + 1) % 3][(col + 1) % 3] === this.player) {
      return true;
    }

    if (row + col === 2 && this.board[(row + 2) % 3][(col + 1) % 3] === this.player && this.board[(row + 1) % 3][(col + 2) % 3] === this.player) {
      return true;
    }
    
    return false;

  }

}
/************************************************************************/
prompt.start();
var newGame = new TicTacToe()
newGame.makeMove();
